# -*- coding: utf-8 -*-

import struct
import time

import smtplib
from email.mime.text import MIMEText

from string import Template

import config
from config import logger

MESSAGE_TYPE_KEEP_ALIVE = 0x00
MESSAGE_TYPE_AUTH = 0x0F
MESSAGE_TYPE_AUTH_RESPONSE = 0x10
MESSAGE_TYPE_SEND_SMS = 0x01
MESSAGE_TYPE_SEND_SMS_RESPONSE = 0x02
MESSAGE_TYPE_SEND_SMS_RESULT = 0x03
MESSAGE_TYPE_SEND_SMS_RESULT_RESPONSE = 0x04
MESSAGE_TYPE_RECEIVE_SMS = 0x05
MESSAGE_TYPE_RECEIVE_SMS_RESPONSE = 0x06
MESSAGE_TYPE_STATUS = 0x07
MESSAGE_TYPE_STATUS_RESPONSE = 0x08
MESSAGE_TYPE_RECEIVE_SMS_RECEIPT = 0x11
MESSAGE_TYPE_RECEIVE_SMS_RECEIPT_RESPONSE = 0x12
MESSAGE_TYPE_SEND_USSD = 0x09
MESSAGE_TYPE_RECEIVE_USSD = 0x0B
MESSAGE_TYPE_RECEIVE_USSD_RESPONSE = 0x0C
MESSAGE_TYPE_CSQ_RSSIR = 0x0D
MESSAGE_TYPE_CSQ_RSSIR_RESPONSE = 0x0E


class DWGMessage(object):
    def __init__(self, client_address=''):

        self.client_address = client_address
        self.ports_number = 0
        self.ID = 0

        if not config.SMTP_HOST:
            raise ValueError('SMTP_HOST is missing')

        if len(config.SMS_EMAIL) < 6:
            raise ValueError('SMS_EMAIL is too short')

    def parse(self, data=[]):
        """
        From the API manual:
        The message is composed of two parts (head and body).
             Length 4 byte Length of body
             ID 16 byte MAC(8 byte，last 2 byte not used)+Time(4byte)+Serial-No(4byte)
             Type 2 byte Type will be introduced later
             Flag 2 byte not used
            Total header length is: 4 + 16 + 2 + 2 = 24
        """
        self.data = data

        if len(self.data) >= 24:
            try:
                self.Length = self.data[0:4]
                self.Length = bytes(self.Length)
                self.Length = struct.unpack('>I', self.Length)[0]  # > big-endian, network, I-unsigned int

                # ID: 16 byte MAC(8 byte，last 2 byte not used)+Time(4byte)+Serial-No(4byte)
                self.ID = self.data[4:20]
                self.MAC = self.ID[0:8]
                self.MAC = bytes(self.MAC)
                self.MAC = struct.unpack('>Q', self.MAC)[0]

                self.Time = self.ID[8:12]
                self.Time = bytes(self.Time)
                self.Time = struct.unpack('>I', self.Time)[0]

                self.SerialNo = self.ID[12:16]
                self.SerialNo = bytes(self.SerialNo)
                self.SerialNo = struct.unpack('>I', self.SerialNo)[0]
                # /ID

                self.Type = self.data[20:22]
                self.Type = bytes(self.Type)
                self.Type = struct.unpack('>H', self.Type)[0]  # H - unsigned short

                self.Flag = self.data[22:24]
                self.Flag = struct.unpack('>H', self.Flag)[0]
            except Exception, e:
                DWGMessage.debug_message(e)

            if len(self.data) > 24:
                self.message_data = self.data[24:]
            else:
                self.message_data = []
        else:  # <24
            self.ID = 0
            self.Length = 0
            self.MAC = 0
            self.Time = 0
            self.SerialNo = 0
            self.Type = -1
            self.Flag = 0
            self.message_data = []

    def __str__(self):
        return "type:%s, length:%s, SerialNo:%s, data:%s" % (str(self.Type), str(self.Length), str(self.SerialNo),
                                                             ":".join(
                                                                 "{0:x}".format(ord(c)) for c in self.message_data))

    def __repr__(self):
        return self.__str__()

    def make_message(self, message_type=None, message_data=[]):
        """
         Length 4 byte Length of body
         ID 16 byte MAC(8 byte，last 2 byte not used)+Time(4byte)+Serial-No(4byte)
         Type 2 byte 
         Flag 2 byte not used
        """
        _packet = ''

        if message_type is None:
            message_type = MESSAGE_TYPE_KEEP_ALIVE

        try:
            # Header start
            _data_length = len(message_data)

            # Length: 4 bytes
            _packet += struct.pack('>I', _data_length)

            '''
            Hua: the ID of ACK response from API server must be the same as gateway request 
            example: while gateway send request with ID 001fxxxxx-001 
            the API server must response the same ID in ACK body 
            if they don't response the same ID, there are more and more requests still waiting response 
            and finally use out all the memory
            '''
            if self.ID == 0:
                ''' Create new ID '''
                ''' ID 16 bytes: MAC(8 byte，last 2 byte not used)+Time(4byte)+Serial-No(4byte) '''
                ''' Mac: 8 bytes '''
                _packet += b'\x01\x02\x03\x04\x05\x06\x00\x00'

                ''' Time: 4 bytes '''
                _time = int(time.time())
                _packet += struct.pack('>I', _time)

                ''' Serial: 4 bytes '''
                _packet += struct.pack('>I', _time % 1000)
                ''' End of ID '''
            else:
                _packet += self.ID

            ''' Type: 2 bytes '''
            _packet += struct.pack('>H', message_type)

            ''' Flag: 2 bytes, always 0 '''
            _packet += struct.pack('>H', 0)
            ''' Header '''

            ''' Data '''
            if len(message_data) > 0:
                for b in message_data:
                    if isinstance(b, basestring):
                        _packet += struct.pack('>B', ord(b))
                    else:
                        _packet += struct.pack('>B', b)
            ''' EOF Data '''

        except Exception, e:
            DWGMessage.debug_message(e.message)

        # _ t = list(_packet)
        return _packet

    def response(self, data):
        self.parse(data)

        response_message = ""

        if self.Type == MESSAGE_TYPE_KEEP_ALIVE:
            DWGMessage.debug_message('MESSAGE_TYPE_KEEP_ALIVE')
            # response_message = self.make_message()

        elif self.Type == MESSAGE_TYPE_AUTH:
            ''' UserId    No more than 16 byte
                Password  No more than 16 byte
            result 1 byte: 0, succeed 1, fail '''

            DWGMessage.debug_message('MESSAGE_TYPE_AUTH')

            _user = None
            _pass = None
            try:
                if self.message_data and len(self.message_data) >= 32:
                    DWGMessage.debug_message('Data:' + str(self.message_data))
                    _user = self.message_data[0:16].strip('\t\n\r' + chr(0))
                    _pass = self.message_data[16:32].strip('\t\n\r' + chr(0))
            except Exception, e:
                DWGMessage.debug_message(e.message)

            if (_user == config.SMS_USER) and (_pass == config.SMS_PASS):
                _result = 0
                DWGMessage.debug_message("AUTH success")
            else:
                _result = 1
                DWGMessage.debug_message("AUTH failed")

            response_message = self.make_message(MESSAGE_TYPE_AUTH_RESPONSE, [_result])

        elif self.Type == MESSAGE_TYPE_STATUS:
            DWGMessage.debug_message('MESSAGE_TYPE_STATUS')
            # Data example: 08 00 00 00 00 01 01 01
            ''' 08                 Count of ports
                00 00             work
                02               not registered
                01 01 01 01 01      no SIM card inserted
            '''

            if len(self.message_data) > 1:
                self.ports_number = ord(self.message_data[0])
                DWGMessage.debug_message('Count of ports:' + str(self.ports_number))
                _ports = self.message_data[1:]
                _number = 0
                for p in _ports:
                    DWGMessage.debug_message('Port #%s, status:%s' % (_number, ord(p)))
                    _number += 1

            _result = 0
            response_message = self.make_message(MESSAGE_TYPE_STATUS_RESPONSE, [_result])

        elif self.Type == MESSAGE_TYPE_CSQ_RSSIR:
            DWGMessage.debug_message('MESSAGE_TYPE_SEND_CSQ_RSSIR')
            ''' Count of Ports  1 byte  Determine how many record below
                        Port 0  1 byte  0~31 csq rssi
            99 unknown  '''
            _result = 0
            response_message = self.make_message(MESSAGE_TYPE_CSQ_RSSIR_RESPONSE,
                                                 [_result])

        # MESSAGE_TYPE_SEND_SMS = 0x01                   SMS -> DWG:  email_sms
        # MESSAGE_TYPE_SEND_SMS_RESPONSE = 0x02          DWG -> SMS
        # MESSAGE_TYPE_SEND_SMS_RESULT = 0x03            DWG -> SMS
        # MESSAGE_TYPE_SEND_SMS_RESULT_RESPONSE = 0x04   SMS -> DWG
        elif self.Type == MESSAGE_TYPE_SEND_SMS_RESPONSE:          # 2 DWG -> SMS
            DWGMessage.debug_message('MESSAGE_TYPE_SEND_SMS_RESPONSE')
            # nothing to response
        elif self.Type == MESSAGE_TYPE_SEND_SMS_RESULT:            # 3 DWG -> SMS
            DWGMessage.debug_message('MESSAGE_TYPE_SEND_SMS_RESULT')
            '''
            Count of Number  1 byte Determine how many record
            Number           24 byte Characters
            Port             1 byte
            Result           1 byte  0, succeed
                                    1, fail
                                    2,timeout
                                    3,bad request
                                    4, port unavailable
                                    5, partial succeed
                                    255, other error
            Count of slice   1 byte A long message will be sliced to several SMS
            Succeeded slices 1 byte
            '''

            # parsing
            # TODO: what todo with this?
            try:
                if len(self.message_data) > 1:
                    _count_number = self.message_data[0]
                    _phone_number = self.message_data[1:25]
                    _port = self.message_data[25:26]
                    _result = self.message_data[26:27]
                    _count_slice = self.message_data[27:28]
                    _slices = self.message_data[28:29]

                    #TODO: p6 what body? Ps: The length of this body is variable.
            except Exception, e:
                DWGMessage.debug_message(e.message)

            _result = 0                                            # 4 SMS -> DWG
            response_message = self.make_message(MESSAGE_TYPE_SEND_SMS_RESULT_RESPONSE,
                                                 [_result])

        elif self.Type == MESSAGE_TYPE_RECEIVE_SMS:
            DWGMessage.debug_message('MESSAGE_TYPE_RECEIVE_SMS')
            ''' Number:  Characters，24 byte  '''
            ''' Type  1 byte  0, SMS 1, MMS(Not Implemented) '''
            ''' Port  1 byte '''
            ''' Timestamp  Characters，15 byte  A string like 2013 1225 13 15 06 '''
            ''' Time zone  1 byte  -12 to +12 '''
            ''' Encoding  1 byte  0, GSM 7BIT 1, UNICODE '''
            ''' Content length  2 byte '''
            ''' Content  Characters '''
            ''' Total min len = 24 + 1 + 1 + 15 + 1 + 1 + 2 = 45'''

            ''' Defaults '''
            _number = ''
            _type = 0
            _port = 0
            _timestamp = ''
            _time_zone = 0
            _encoding = 0
            _content_len = 0
            _content = ''

            _time_year = 0
            _time_month = 0
            _time_day = 0
            _time_hour = 0
            _time_min = 0
            _time_sec = 0

            if len(self.message_data) >= 45:
                _result = 0
                _len = 0

                try:
                    _number = str(self.message_data[0:24]).strip('\t\n\r' + chr(0))
                    _type = self.message_data[24:25]  # 0 - sms always as per doc
                    _port = self.message_data[25:26]
                    _timestamp = self.message_data[26:41]
                    if len(_timestamp) >= 14:
                        _time_year = _timestamp[0:4]
                        _time_month = _timestamp[4:6]
                        _time_day = _timestamp[6:8]
                        _time_hour = _timestamp[8:10]
                        _time_min = _timestamp[10:12]
                        _time_sec = _timestamp[12:14]
                        _timestamp = _time_year + '/' + _time_month + '/' + _time_day + ' ' + _time_hour + ':' + _time_min + ':' + _time_sec

                    _time_zone = self.message_data[41:42]
                    _encoding = ord(self.message_data[42:43])  # 0 - 7bit, 1 - unicode
                    _len = self.message_data[43:45]

                    _content = self.message_data[45:]
                    _len = struct.unpack('>H', _len)[0]
                    _port = struct.unpack('>b', _port)[0]

                except Exception, e:
                    DWGMessage.debug_message(e)

                DWGMessage.debug_message('SMS Number:%s' % _number)
                DWGMessage.debug_message('SMS Encoding:%s' % str(_encoding))
                DWGMessage.debug_message('SMS Time:%s' % _timestamp)
                DWGMessage.debug_message('SMS Length :%s' % _len)
                DWGMessage.debug_message('SMS Content :%s' % _content)
            else:
                _result = 1

            response_message = self.make_message(MESSAGE_TYPE_RECEIVE_SMS_RESPONSE, [_result])

            if _result == 0:
                if _encoding == 1:  # UTF-8
                    _content = DWGMessage.unicode_bytes_string(_content)
                    # DWGMessage.debug_message('SMS Content UTF8:%s' % (_content))

                DWGMessage.debug_message('Sending email')
                ''' Send email '''
                sender = config.SMS_EMAIL
                recipient = config.SMS_EMAIL

                config.email_template_values['sender_phone'] = _number
                config.email_template_values['timestamp'] = _timestamp
                config.email_template_values['sms_text'] = _content
                config.email_template_values['dwg_ip'] = str(self.client_address)
                config.email_template_values['dwg_card_number'] = str(_port)

                # subject = u'SMS2Email'
                subject = Template(config.EMAIL_SUBJECT_TEMPLATE).substitute(config.email_template_values)
                # body = _content + ' @' + _timestamp + ' ' + _number
                body = Template(config.EMAIL_MESSAGE_TEMPLATE).substitute(config.email_template_values)

                DWGMessage.send_email(sender, recipient, subject, body)
            else:
                DWGMessage.debug_message('Receive SMS length error: ' + str(len(self.message_data)))

        elif self.Type == MESSAGE_TYPE_RECEIVE_USSD:
            DWGMessage.debug_message('MESSAGE_TYPE_RECEIVE_USSD')
            ''' Port         1 byte
                Status       1 byte     0,No further user action required
                                        1, Further user action required
                                        2,USSD terminated by network
                                        4, Operation not supported
                Content Length  2 byte
                Encoding        1 byte  1, UNICODE
                Content         Characters  Could be empty
                
                Total min length: 1 + 1 + 2 + 1 = 5 bytes   
            '''

            if len(self.message_data) >= 5:
                _result = 0

                try:
                    _port = ord(self.message_data[0:1])
                    _status = ord(self.message_data[1:2])
                    _len = self.message_data[2:4]
                    _encoding = ord(self.message_data[4:5])
                    _content = self.message_data[5:]

                    _len = struct.unpack('>H', _len)[0]

                    DWGMessage.debug_message('USSD Port:%s' % (str(_port)))
                    DWGMessage.debug_message('USSD Status:%s' % (str(_status)))
                    DWGMessage.debug_message('USSD Length:%s' % (str(_len)))
                    DWGMessage.debug_message('USSD Encoding:%s' % (str(_encoding)))
                    DWGMessage.debug_message('USSD Content :%s' % (_content))

                except Exception as e:
                    DWGMessage.debug_message(e)
            else:
                _result = 1

            response_message = self.make_message(MESSAGE_TYPE_RECEIVE_USSD_RESPONSE, [_result])

            if _result == 0:
                _content = DWGMessage.unicode_bytes_string(_content)
                DWGMessage.debug_message('Sending email')
                ''' Send email '''
                sender = config.SMS_EMAIL
                recipient = config.SMS_EMAIL
                subject = u'USSD SMS'
                body = _content

                DWGMessage.send_email(sender, recipient, subject, body)
            else:
                DWGMessage.debug_message('Receive USSD length error: ' + str(len(self.message_data)))

        elif self.Type == MESSAGE_TYPE_SEND_USSD:
            DWGMessage.debug_message('MESSAGE_TYPE_SEND_USSD')
        elif self.Type == MESSAGE_TYPE_RECEIVE_SMS_RECEIPT:
            DWGMessage.debug_message('MESSAGE_TYPE_RECEIVE_SMS_RECEIPT')
        elif self.Type == MESSAGE_TYPE_RECEIVE_SMS_RECEIPT_RESPONSE:
            DWGMessage.debug_message('MESSAGE_TYPE_RECEIVE_SMS_RECEIPT_RESPONSE')

        else:
            DWGMessage.debug_message('response: UNKNOWN PACKET TYPE' + str(self.Type))

        return response_message

    def message_data_hex_dump(self):
        DWGMessage.debug_message(":".join("{0:x}".format(ord(c)) for c in self.message_data))

    def email_sms(self, phone, message, count=1, port=255):
        """Send SMS Request, 4 step action init by SMS server"""
        DWGMessage.debug_message('MESSAGE_TYPE_SEND_SMS')
        DWGMessage.debug_message('text: %s' % message)

        '''
        Port               1 byte 255 for any port
        Encoding           1 byte 0, GSM 7bit 1, UNICODE
        Type               1 byte 0, SMS 1,MMS(Not Implemented)
        Count of Number    1 byte No more than 100
        Number Characters, 24 bytes
        Content Length     2 bytes No more than 1530(gsm 7bit) or 1340 (unicode)
        SMS Content        Characters
        '''
        _packet = []

        # Port: 1 byte  255 for any port
        try:
            _port = int(port)
            if _port > 255:
                _port = 255
        except:
            return None

        # 0, GSM 7bit 1, UNICODE
        _encoding = 1

        # Type:  1 byte  0,SMS 1,MMS(Not Implemented)
        _type = 0

        # Count of Number: 1 byte  No more than 100
        try:
            _count = int(count)
            if _count > 100:
                _count = 100
        except:
            return None

        # Number Characters, 24 bytes
        # Single number possible, use first if multiple
        import re
        _number = None
        try:
            _number = phone.split(',')[0]
            _number = re.sub(r'[^\d\+]+', '', _number)
            _number = _number[:24]
            if len(_number) > 24:
                _number = _number[:24]
            else:
                _number = _number.ljust(24, chr(0))  # need 24 bytes
        except:
            return None

        # Content Length: 2 byte  No  more  than  1530(gsm  7bit) or 1340 (unicode)
        message = message[:670]  # 1340/2
        _mess = DWGMessage.string_unicode_bytes(message)
        _len = len(_mess)

        _packet.append(_port)
        _packet.append(_encoding)
        _packet.append(_type)
        _packet.append(_count)
        _packet.extend(list(_number))
        _packet.extend(struct.pack('>H', _len))
        _packet.extend(list(_mess))

        # TODO: Check me
        message = self.make_message(MESSAGE_TYPE_SEND_SMS, _packet)
        return message

    @staticmethod
    def debug_message(mess):
        if config.DEBUG > 0:
            logger.debug(mess)
            # print(mess)

    @staticmethod
    def send_email(sender, recipient, subject, msg):
        try:
            msg = MIMEText(msg, 'plain', 'UTF-8')
            msg.set_charset('utf-8')

            msg['Subject'] = subject
            msg['From'] = sender
            msg['To'] = recipient

            s = smtplib.SMTP(host=config.SMTP_HOST, port=config.SMTP_PORT)

            if config.SMTP_TLS == 1:
                s.starttls()

            if config.DEBUG:
                s.set_debuglevel(1)
            else:
                s.set_debuglevel(False)

            if config.SMTP_USER and config.SMTP_PASS:
                s.login(config.SMTP_USER, config.SMTP_PASS)

            s.sendmail(sender, [recipient], msg.as_string())
        finally:
            s.quit()

    @staticmethod
    def unicode_bytes_string(msg):
        # a= b'\x04\x22\x04\x35\x04\x41\x04\x42\x00\x34'
        # _content = struct.unpack('>'+ str(len(_content)/2) + 'h', _content)
        # (1058, 1077, 1089, 1090, 52)
        # _content = "".join([unichr(i) for i in _content])
        # u'\u0422\u0435\u0441\u04424'

        i = 0
        _len = len(msg)
        _res = []
        while i + 1 < _len:
            _res.extend(struct.unpack('>H', msg[i:i + 2]))
            i += 2
        _content = u''
        try:
            _content = u''.join([unichr(i) for i in _res])
        except Exception, e:
            DWGMessage.debug_message(e)

        return _content.encode('utf-8')

    @staticmethod
    def string_unicode_bytes(msg):
        msg = msg.encode('utf-8')
        _res = ''
        try:
            for c in msg:
                _res += struct.pack('>H', ord(c))
        except Exception, e:
            DWGMessage.debug_message(e)

        return _res