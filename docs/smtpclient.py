# -*- coding: utf-8 -*-

import smtplib
import email.utils
from email.mime.text import MIMEText

recipient_email = 'butsyk.anton@gmail.com'
author_email = 'butsyk.anton@gmail.com'

# Create the message
msg = MIMEText('This is the body of the message.')
msg['To'] = '+380506537314'
#msg['To'] = email.utils.formataddr(('Author', author_email))
msg['From'] = email.utils.formataddr(('Author', author_email))
msg['Subject'] = 'Simple test message'

server = smtplib.SMTP('127.0.0.1', 2525)
server.set_debuglevel(True)  # show communication with the server
try:
    server.sendmail(author_email, [ recipient_email ], msg.as_string())
finally:
    server.quit()