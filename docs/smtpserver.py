# -*- coding: utf-8 -*-

import smtpd
import asyncore
import threading


# RFC 5321, RFC 1870 SIZE extension
class CustomSMTPServer(smtpd.SMTPServer):
    emails = []

    def process_message(self, peer, mailfrom, rcpttos, data):
        """
        print 'Receiving message from:', peer
        print 'Message addressed from:', mailfrom
        print 'Message addressed to  :', rcpttos
        print 'Message length        :', len(data)
        """

        res = data.split("\n\n")
        if len(res) == 2 and res[1]:
            # res[0] header
            # res[1] message
            CustomSMTPServer.emails.append(res[1])

        return


class CustomSMTPReceiver(object):
    def __init__(self, server_address, queue):
        self.server_address = server_address
        self.queue = queue

    def start(self):
        """Start the listening service"""
        # here I create an instance of the SMTP server, derived from  asyncore.dispatcher
        self.smtp = CustomSMTPServer(self.server_address, None)
        # and here I also start the asyncore loop, listening for SMTP connection, within a thread
        # timeout parameter is important, otherwise code will block 30 seconds after the smtp channel has been closed
        self.thread = threading.Thread(target=asyncore.loop, kwargs={'timeout': 1})
        self.thread.start()

    def stop(self):
        """Stop listening now to port 25"""
        # close the SMTPserver to ensure no channels connect to asyncore
        self.smtp.close()
        # now it is save to wait for the thread to finish, i.e. for asyncore.loop() to exit
        self.thread.join()

    def count(self):
        """Return the number of emails received"""
        return len(self.smtp.emails)

    def get(self):
        """Return all emails received so far"""
        return self.smtp.emails
