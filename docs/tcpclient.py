# -*- coding: utf-8 -*-

import socket
import time

a = 'test3'


HOST, PORT = "localhost", 9999
#data = " ".join(sys.argv[1:])

''' Example from API '''
test_packet = [0, 0, 0, 0x26, 00, 01, 02, 03, 04, 05, 00, 00, 0x1f, 0x5c, 0xb8, 0x18, \
     00, 00, 00, 06, 00, 01, 00, 00, 00, 01, 00, 01, 0x31, 0x35, 0x30, 0x38, \
     0x38, 0x38, 0x38, 0x39, 0x39, 0x39, 0x39, 0x00, 0x00, 00, 00, 00, 00, 00, 00, 00, \
     00, 00, 00, 00, 00, 0x08, 00, 0x31, 00, 0x32, 00, 0x33, 00, 0x34]

''' Example from API '''
test_packet2 = [00, 00, 00, 0x31, 00, 01, 02, 03, 04, 05, 00, 00, 0x0d, 0x2e, 0x40, 0xaf, \
     00, 00, 00, 06, 00, 01, 00, 00, 00, 01, 00, 01, 0x31, 0x35, 0x30, 0x38, \
     0x38, 0x38, 0x38, 0x39, 0x39, 0x39, 0x39, 0x00, 0x00, 00, 00, 00, 00, 00, 00, 00, \
     00, 00, 00, 00, 00, 0x08, 00, 0x31, 00, 0x32, 00, 0x33, 00, 0x34]

''' Example from API '''
auth_data = [00, 00, 00, 0x20, 
             00, 01, 02, 03, 04, 05, 00, 00,   0x0d, 0x2e, 0x3f, 0xfd,   00, 00, 00, 02,\
             00, 0x0f,\
             00, 00,\
             0x61, 0x64, 0x6D, 0x69, 0x6E, 00, 00, 00,\
             00, 00, 00, 00, 00, 00, 00, 00, 0x61, 0x64, 0x6D, 0x69, 0x6E, 00, 00, 00,\
             00, 00, 00, 00, 00, 00, 00, 00]

#data = ''.join([chr(s) for s in auth_data])
#'admin\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00
#admin\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

data = ''.join([chr(s) for s in auth_data])
 

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    sock.connect((HOST, PORT))
    while True:
        sock.sendall(data + "\n")
        received = sock.recv(1024)

        print "Sent:{}".format(data)
        print "Recv:{}".format(received)

        time.sleep(2)
except:
    sock.close()




