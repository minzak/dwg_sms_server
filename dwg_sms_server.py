#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

from daemon import Daemon
from smsserver import sms_main


class MyDaemon(Daemon):
    def run(self):
        sms_main()

if __name__ == "__main__":
    path = os.path.dirname(os.path.realpath(__file__))

    daemon = MyDaemon(path + '/dwg_sms_server.pid',
                      stdout=path+'/dwg_sms_server.log',
                      stderr=path+'/dwg_sms_server.log')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
