#!/usr/bin/env python
# -*- coding: utf-8 -*-

import threading
import SocketServer
import Queue

import smtpd
import asyncore

import config
from config import logger
from dwgmessage import DWGMessage


q_emails = Queue.Queue()


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def setup(self):
        """Start a new thread to process the email q"""
        try:
            self._email_thread = threading.Thread(target=self.process_email_thread,
                                                  args=(self.request, self.client_address))
            self._email_thread.daemon = True
            self._email_thread.start()
        except:
            self._email_thread = None

    def handle(self):
        while True:
            data = self.request.recv(2048)
            if not data:
                break

            dwg_message = DWGMessage(self.client_address)
            response = dwg_message.response(data)
            self.request.sendall(response)

    def finish(self):
        try:
            if self._email_thread and self._email_thread.isAlive():
                self._email_thread.join(0.1)
        except:
            pass

    def process_email_thread(self, request, client_address):
        while True:
            (phone_number, _mess) = q_emails.get()  # block here
            try:
                if request:
                    dwg_message = DWGMessage(client_address)
                    response = dwg_message.email_sms(phone=phone_number,
                                                     message=_mess)
                    if response:
                        request.sendall(response)  # TODO: random client accepts
            finally:
                q_emails.task_done()   # release mutex


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


# RFC 5321, RFC 1870 SIZE extension
class CustomSMTPServer(smtpd.SMTPServer):
    def process_message(self, peer, mailfrom, rcpttos, data):
        """peer - Receiving message from
           mailfrom - Message addressed from
           rcpttos - Message addressed to"""
        res = data.split("\n\n")
        if len(res) == 2 and res[1]:
            phone = None
            for _h in res[0].split("\n"):
                if _h.lower().startswith('to:'):
                    phone = _h[4:]
                    break

            if phone:
                q_emails.put((phone, res[1]))
        return


def sms_main():
    logger.debug("Starting SMS server")
    tcp_server = ThreadedTCPServer((config.SERVER_HOST, config.SERVER_PORT), ThreadedTCPRequestHandler)

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    tcp_thread = threading.Thread(target=tcp_server.serve_forever)
    # Exit the server thread when the main thread terminates
    tcp_thread.daemon = True
    tcp_thread.start()

    if hasattr(config, 'SMTPD_HOST') and config.SMTPD_HOST and config.SMTPD_PORT:
        logger.debug("Starting SMTP server")
        try:
            smtp_server = CustomSMTPServer((config.SMTPD_HOST, config.SMTPD_PORT), None)
            smtp_thread = threading.Thread(target=asyncore.loop, kwargs={'timeout': 1})
            smtp_thread.daemon = True
            smtp_thread.start()

            # smtp_server.close()
            # smtp_thread.join()

        except Exception, e:
            logger.debug("SMTP server error: %s" % e.message)

    tcp_thread.join()
    tcp_server.shutdown()
   
    logger.debug("Stop dwg_sms_server")


if __name__ == "__main__":
    sms_main()