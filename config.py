# -*- coding: utf-8 -*-

DEBUG = 1  # Set to 0 for production

SERVER_HOST = '127.0.0.1'  # IP address where to listen for incoming
SERVER_PORT = 9999         # Tcp port

SMS_USER = 'admin' 
SMS_PASS = 'admin'
SMS_EMAIL = 'email@email.com'  # Change this to your email
 
SMTP_HOST = '127.0.0.1'  # Relay host for email messages, for example: smtp.gmail.com
SMTP_PORT = 25           # for example: 587
SMTP_USER = ''           # email address to send to
SMTP_PASS = ''           # empty - do not use login.
SMTP_TLS = 0             # 1 - TLS enabled, for example: google smtp requires TLS so =1

EMAIL_SUBJECT_TEMPLATE = 'SMS from:$sender_phone at $timestamp via $dwg_ip'
EMAIL_MESSAGE_TEMPLATE = 'From:$sender_phone, text:$sms_text, on:$timestamp'

email_template_values = dict(
    sender_phone='',
    timestamp='',
    sms_text='',
    dwg_ip='',
    dwg_card_number=''
)

SMTPD_HOST = '127.0.0.1'  # IP address where to listen for smtp incoming connections
SMTPD_PORT = 2525         # Smtp port

import logging
logging.basicConfig(filename='dwg_sms_server.log', level=logging.DEBUG, format='%(asctime)s %(message)s') 
logger = logging.getLogger()
